package fsys

import "golang.org/x/sys/unix"

func open(path string, mode int, perm uint32) (file baseFile, stat Stat, err error) {
	var fd int

	// Open the file descriptor at path.
	err = ignoreEINTR(func() error {
		fd, err = unix.Open(path, mode, perm)
		return err
	})
	if err != nil {
		return
	}

	// Wrap file descriptor.
	file = newBaseFile(fd, path)

	// Stat fd for type info.
	stat, err = file.Stat()

	if err != nil {
		// Unusable file.
		_ = file.Close()
	}

	return
}

// baseFile provides a base set of helper methods for a file descriptor at file system path.
type baseFile struct {
	path string
	baseFD
}

// newBaseFile returns a new base file from given path and file descriptor.
func newBaseFile(fd int, path string) baseFile {
	return baseFile{
		path: path,
		baseFD: baseFD{
			fd: to_internal_fd(fd),
		},
	}
}

// Stat will call fstat() on file descriptor and return the results.
func (f *baseFile) Stat() (stat Stat, err error) {
	if f.fd == 0 {
		return stat, ErrAlreadyClosed
	}
	return stat, ignoreEINTR(func() error {
		return unix.Fstat(f.FD(), (*unix.Stat_t)(&stat))
	})
}

// Chdir will call fchdir on file descriptor.
func (f *baseFile) Chdir() (err error) {
	if f.fd == 0 {
		return ErrAlreadyClosed
	}
	return ignoreEINTR(func() error {
		return unix.Fchdir(f.FD())
	})
}

// Chmod will call fchmod() on file descriptor.
func (f *baseFile) Chmod(mode uint32) (err error) {
	if f.fd == 0 {
		return ErrAlreadyClosed
	}
	return ignoreEINTR(func() error {
		return unix.Fchmod(f.FD(), mode)
	})
}

// Chown will call fchown() on file descriptor.
func (f *baseFile) Chown(uid, gid int) (err error) {
	if f.fd == 0 {
		return ErrAlreadyClosed
	}
	return ignoreEINTR(func() error {
		return unix.Fchown(f.FD(), uid, gid)
	})
}

// Path returns the filesystem path associated with this file.
func (f *baseFile) Path() string {
	return f.path
}
