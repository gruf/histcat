package fsys

import (
	"reflect"
	"runtime"
	"unsafe"

	"golang.org/x/sys/unix"
)

// Stdin, Stdout, and Stderr return File structures pointing to standard input,
// standard output and standard error. These are returned as new objects each time
// due to the lack of concurrency protection within the File structure itself.
func Stdin() *File  { return &File{newBaseFile(unix.Stdin, "/dev/stdin")} }
func Stdout() *File { return &File{newBaseFile(unix.Stdout, "/dev/stdout")} }
func Stderr() *File { return &File{newBaseFile(unix.Stderr, "/dev/stderr")} }

// Stat is a type alias to unix.Stat_t.
type Stat unix.Stat_t

// File represents a (non-dir) file descriptor, providing syscall helper methods.
// Note that it skips a lot of the nuances offered by os.File{} for other file types,
// and provides no form of protection for concurrent use.
type File struct{ baseFile }

// OpenFile will open file descriptor at path with mode and permissions (must be regular file / symlink to).
func OpenFile(path string, mode int, perm uint32) (file *File, stat Stat, err error) {
	// Open file descriptor at path with opts.
	base, stat, err := open(path, mode, perm)
	if err != nil {
		return nil, stat, err
	}

	// Check this is a non-dir file.
	if stat.Mode&unix.S_IFDIR != 0 {
		_ = base.Close()
		return nil, stat, ErrUnsupported
	}

	// Return wrapped file descriptor.
	return newFile(base), stat, err
}

// NewFile returns a new File instance from given file descriptor and path.
func NewFile(fd int, filepath string) *File {
	return newFile(newBaseFile(fd, filepath))
}

// newFile wraps a baseFD in File and adds a runtime finalizer.
func newFile(file baseFile) *File {
	f := &File{file}

	// Ensure file gets closed on dealloc.
	runtime.SetFinalizer(f, func(f *File) {
		_ = f.Close()
	})

	return f
}

// Read will read bytes into given buffer from file descriptor.
func (f *File) Read(b []byte) (nn int, err error) {
	if f.fd == 0 {
		return 0, ErrAlreadyClosed
	}
	return syscall_read(f.FD(), b)
}

// Write will write given bytes to file descriptor, using as many write() calls as necessary.
func (f *File) Write(b []byte) (nn int, err error) {
	if f.fd == 0 {
		return 0, ErrAlreadyClosed
	}
	return syscall_write(f.FD(), b)
}

// WriteString calls .Write() after converting string to byte slice.
func (f *File) WriteString(s string) (int, error) {
	var b []byte
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh.Data = sh.Data
	bh.Len = sh.Len
	bh.Cap = sh.Len
	return f.Write(b)
}

// Seek will call seek() on file descriptor with given offset and return new offset.
func (f *File) Seek(offset int64, whence int) (off int64, err error) {
	if f.fd == 0 {
		return 0, ErrAlreadyClosed
	}
	return off, ignoreEINTR(func() error {
		off, err = unix.Seek(f.FD(), offset, whence)
		return err
	})
}

// Fallocate will call fallocate() with given mode, offset and legnth on file descriptor.
func (f *File) Fallocate(mode uint32, off int64, len int64) error {
	if f.fd == 0 {
		return ErrAlreadyClosed
	}
	return ignoreEINTR(func() error {
		return unix.Fallocate(f.FD(), mode, off, len)
	})
}

// Sync will call fsync() on file descriptor.
func (f *File) Sync() error {
	if f.fd == 0 {
		return ErrAlreadyClosed
	}
	return ignoreEINTR(func() error {
		return unix.Fsync(f.FD())
	})
}
