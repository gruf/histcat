package main

// trimSuffix will trim byte suffix 'c' from slice 'b'.
func trimSuffix(b []byte, c byte) []byte {
	if l := len(b) - 1; l >= 0 && b[l] == c {
		return b[:l]
	}
	return b
}
