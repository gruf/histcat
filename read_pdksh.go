package main

import (
	"bufio"
	"errors"
	"io"

	"codeberg.org/gruf/go-fsys"
)

const (
	pdksh_magic = "\xab\xcd"
	pdksh_cmd   = '\xff'
)

func is_pdksh(data []byte) bool {
	return len(data) >= 2 && string(data[0:2]) == pdksh_magic
}

func read_from_pdksh(br *bufio.Reader, w *fsys.File) error {
	// Discard pdksh header magic bytes
	_, _ = br.Discard(len(pdksh_magic))

	for done := false; !done; {
		var line []byte

		// Read up to next command byte
		_, err := br.ReadSlice(pdksh_cmd)

		switch err {
		case nil:
			// no error
		case bufio.ErrBufferFull:
			return errors.New("missing pdksh cmd byte")
		case io.EOF:
			return nil
		default:
			return err
		}

		// Drop 4-octet line number
		_, _ = br.Discard(4)

		for {
			// Read up to next NUL terminator
			next, err := br.ReadSlice('\x00')

			switch err {
			case nil:
				// no error
			case bufio.ErrBufferFull:
				// keep reading
			case io.EOF:
				done = true
			default:
				return err
			}

			if len(line) > 0 {
				// Multiple reads, append 'next'
				line = append(line, next...)
			} else {
				// First read
				line = next
			}

			break
		}

		// Trim line's NULL terminator
		line = trimSuffix(line, '\x00')

		// Skip empty lines
		if len(line) == 0 {
			continue
		}

		// Append a newline char
		line = append(line, '\n')

		// Write line to output file descriptor
		if _, err := w.Write(line); err != nil {
			return err
		}
	}

	return nil
}
