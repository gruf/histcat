package main

import (
	"bufio"
	"errors"
	"io"
	"runtime/debug"

	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"golang.org/x/sys/unix"
)

var (
	// std file descriptors.
	stdout = fsys.Stdout()
	stderr = fsys.Stderr()
)

func main() {
	var code int

	// Run main application
	if err := run(); err != nil {
		stderr.WriteString("FATAL: " + err.Error() + "\n")
		code = 1
	}

	// Exit with code
	unix.Exit(code)
}

func run() error {
	const buf = 16 * 1024

	var (
		format string
		ftype  bool
	)

	// Declare runtime flags to global fflag.FlagSet.
	fflag.StringVar(&format, "f", "format", "", "Parse as history format (bash,mksh,fish...)")
	fflag.BoolVar(&ftype, "t", "type", false, "Simply output history file format")
	fflag.Version(version())
	fflag.Help()

	// Parse runtime flags.
	args, err := fflag.Parse()
	if err != nil {
		return err
	}

	// Allocate buffered reader of size.
	br := bufio.NewReaderSize(nil, buf)

	if len(args) == 0 {
		// Default input is /dev/stdin.
		args = []string{"/dev/stdin"}
	}

	// Handle each input path.
	for _, path := range args {

		// Open input file at given path.
		file, _, err := fsys.OpenFile(path, 0, 0)
		if err != nil {
			return wrap_err(err, "error opening "+path)
		}

		// NOTE: we leave file descr close
		// on error up to Linux to handle.

		// Reset buf to
		// input reader.
		br.Reset(file)

		// Rescope 'format'
		// in case it changes.
		format := format

		if format == "" {
			// No history format given.
			format, err = parse_format(br)
			if err != nil {
				return wrap_err(err, "error parsing "+path)
			}
		}

		if ftype {
			// Only print the history format.
			stdout.WriteString(format + "\n")
		} else {
			// Write parsed input to output, using shell format.
			if err := read_from(format, br, stdout); err != nil {
				return wrap_err(err, "error reading "+path)
			}
		}

		// Done with file.
		_ = file.Close()
	}

	return nil
}

func version() string {
	var flags, commit, time string
	build, _ := debug.ReadBuildInfo()
	for i := 0; i < len(build.Settings); i++ {
		switch build.Settings[i].Key {
		case "-gcflags":
			flags += ` -gcflags="` + build.Settings[i].Value + `"`
		case "-ldflags":
			flags += ` -ldflags="` + build.Settings[i].Value + `"`
		case "-tags":
			flags += ` -tags="` + build.Settings[i].Value + `"`
		case "vcs.revision":
			commit = build.Settings[i].Value
			if len(commit) > 8 {
				commit = commit[:8]
			}
		case "vcs.time":
			time = build.Settings[i].Value
		}
	}
	return "commit=" + commit + "\n" +
		"build=" + build.GoVersion + flags + "\n" +
		"time=" + time
}

func parse_format(br *bufio.Reader) (string, error) {
	// Peek the first 10 bytes
	data, err := br.Peek(10)
	if err != nil && err != io.EOF {
		return "", err
	}

	switch {
	// Look for pdksh
	// magic header bytes
	case is_pdksh(data):
		return "pdksh", nil

	// Look for ksh93
	// magic header bytes
	case is_ksh93(data):
		return "ksh93", nil

	// Look for firsh line
	// of fish history yaml
	case is_fish(data):
		return "fish", nil

	// Default assume plaintext
	default:
		return "plaintext", nil
	}
}

func read_from(shell string, br *bufio.Reader, w *fsys.File) error {
	switch shell {
	// ksh93 binary format
	case "ksh93":
		return read_from_ksh93(br, w)

	// pdksh binary format
	case "pdksh", "mksh":
		return read_from_pdksh(br, w)

	// Shells using plaintext format
	case "plaintext", "bash", "loksh", "oksh":
		_, err := br.WriteTo(w)
		return err

	// Fish yaml format
	case "fish":
		return read_from_fish(br, w)

	// Unknown
	default:
		return errors.New("unrecognized shell: " + shell)
	}
}

func wrap_err(err error, msg string) error {
	return errors.New(msg + ": " + err.Error())
}
