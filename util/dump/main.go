package main

import (
	"io"
	"os"
	"strconv"
)

func main() {
	b, _ := io.ReadAll(os.Stdin)
	s := strconv.Quote(string(b))
	os.Stdout.WriteString(s)
}
