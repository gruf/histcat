package main

import (
	"bufio"
	"io"

	"codeberg.org/gruf/go-fsys"
)

const (
	// ksh93_magic = H_UNDO + H_VERSION
	ksh93_undo = '\x81'
	ksh93_v0   = '\x00'
	ksh93_v1   = '\x01'
)

func is_ksh93(data []byte) bool {
	return len(data) >= 2 && data[0] == ksh93_undo &&
		(data[1] == ksh93_v0 || data[1] == ksh93_v1)
}

func read_from_ksh93(br *bufio.Reader, w *fsys.File) error {
	// Discard ksh93 header magic bytes
	_, _ = br.Discard(2)

	for done := false; !done; {
		var line []byte

		for {
			// Read up to next NUL terminator
			next, err := br.ReadSlice('\x00')

			switch err {
			case nil:
				// no error
			case bufio.ErrBufferFull:
				// keep reading
			case io.EOF:
				done = true
			default:
				return err
			}

			if len(line) > 0 {
				// Multiple reads, append 'next'
				line = append(line, next...)
			} else {
				// Singular read
				line = next
			}

			break
		}

		// Trim line's NULL terminator
		line = trimSuffix(line, '\x00')

		// Skip empty lines
		if len(line) == 0 {
			continue
		}

		// Write line to output file descriptor
		if _, err := w.Write(line); err != nil {
			return err
		}
	}

	return nil
}
