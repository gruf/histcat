# histcat

Shell history files are stored in a variety of formats. From plain text, to yaml, to custom binary formats.

Histcat allows you to read (most) shell history files as plain-text.

Supports:
- bash (plaintext)
- ksh93 (binary)
- pdksh (binary)
- mksh (binary)
- loksh (plaintext)
- oksh (plaintext)
- fish (yaml)

Does NOT support:
- blue shells
- red shells
- green shells
