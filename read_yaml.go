package main

import (
	"bufio"
	"io"

	"codeberg.org/gruf/go-fsys"
)

const (
	fish_magic = "- cmd: "
)

func is_fish(data []byte) bool {
	return len(data) >= 7 && string(data[0:7]) == fish_magic
}

func read_from_fish(br *bufio.Reader, w *fsys.File) error {
	for done := false; !done; {
		var line []byte

		for {
			// Read up to next new-line
			next, err := br.ReadSlice('\n')

			switch err {
			case nil:
				// no error
			case bufio.ErrBufferFull:
				// keep reading
			case io.EOF:
				done = true
			default:
				return err
			}

			if len(line) > 0 {
				// Multiple reads, append 'next'
				line = append(line, next...)
			} else {
				// Singular read
				line = next
			}

			break
		}

		// If does not have
		// yaml prefix, skip
		if is_fish(line) {
			continue
		}

		// Trim prefix and unescape
		line = line[len(fish_magic):]
		line = unescape_yaml(line)

		// Skip empty lines
		if len(line) == 0 {
			continue
		}

		// Write line to output file descriptor
		if _, err := w.Write(line); err != nil {
			return err
		}
	}

	return nil
}

// yamlEscapeChars contains conversions of
// escaped characters to their unescaped form.
var yaml_escape_chars = [256]byte{
	'a': '\a',
	'b': '\b',
	'f': '\f',
	'n': '\n',
	'r': '\r',
	't': '\t',
	'v': '\v',
}

// unescape_yaml will unescape a yaml string in place in line buffer 'b'.
func unescape_yaml(b []byte) []byte {
	var delim bool

	for i := 0; i < len(b); {
		if delim {
			// Unset delim
			delim = false

			// Unescape YAML chars
			if c := yaml_escape_chars[b[i]]; c != 0 {
				b[i-1] = c
			}

			// Drop the extra '\' char
			_ = copy(b[i:], b[i+1:])
			b = b[:len(b)-1]

			continue
		}

		if b[i] == '\\' {
			// Set delim
			delim = true
		}

		// Incr.
		i++
	}

	return b
}
